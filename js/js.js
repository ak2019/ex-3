var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 30, window.innerWidth/window.innerHeight, 0.1, 1000 );
// mouse controls
var controls = new THREE.OrbitControls( camera );
controls.autoRotate = true;

var renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );


// load a texture, set wrap mode to repeat
var loader = new THREE.TextureLoader();
loader.crossOrigin = '';
var texture = loader.load( "http://packageonly.tk/1.jpg" );
texture.wrapS = THREE.RepeatWrapping;
texture.wrapT = THREE.RepeatWrapping;
texture.repeat.set( 4, 4 );

// add cube
var geometry = new THREE.BoxGeometry( 1, 1, 1 );
var material = new THREE.MeshPhongMaterial( {map:texture, specular: 0xFFFFFF,
    shininess: 10,} );
var cube = new THREE.Mesh( geometry, material );
scene.add( cube );

let light1 = new THREE.PointLight(0xeeffee, 0.8);
    light1.position.set(-5, 2, 2);
    scene.add(light1);
    
let light2 = new THREE.PointLight(0xffeeff, 0.8);
    light2.position.set(5, 2, -2);
    scene.add(light2);    

let light3 = new THREE.PointLight(0xffeeff, 0.8);
    light3.position.set(5, 4, -4);
    scene.add(light3); 

let light4 = new THREE.PointLight(0xffeeff, 0.8);
    light4.position.set(-5, 4, -4);
    scene.add(light4);     

material.map = texture;

camera.position.z = 5;

var render = function () {
  requestAnimationFrame( render );

  controls.update();
  renderer.render(scene, camera);
};

render();


